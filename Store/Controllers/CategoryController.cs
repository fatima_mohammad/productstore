﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class CategoryController : Controller
    {
        // GET: /<controller>/
        private readonly IElctronicStoreRepository<Category> categoryRepository;
        private readonly IHostingEnvironment hosting;

        public CategoryController(IElctronicStoreRepository<Category> categoryRepository,
            IHostingEnvironment hosting)
        {
            this.categoryRepository = categoryRepository;
            this.hosting = hosting;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var categorys = categoryRepository.List();
            return View(categorys);
        }
        public IActionResult Details(int id)
        {
            var category = categoryRepository.Find(id);

            return View(category);
        }
        public IActionResult Create()
        {
            
            return View();
        }
        [HttpPost]
        public IActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;
                if (category.File != null)
                {
                    string uploads = Path.Combine(hosting.WebRootPath, "uploads");
                    fileName = category.File.FileName;
                    //مسار الملف كاملا
                    string fullPath = Path.Combine(uploads, fileName);
                    // في ل fullPathحفظ الملف
                    category.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    category.ImageUrl = fileName;
                }
                categoryRepository.Add(category);
                return RedirectToAction(nameof(Index));
            }
            ModelState.AddModelError("", "You have to fill all required fields!");
            return View();

        }

        public IActionResult Edit(int id)
        {
            var category = categoryRepository.Find(id);
            return View(category);
        }
        [HttpPost]
        public IActionResult Edit(int id, Category category)
        {
            try
            {
                string fileName = string.Empty;
                if (category.File != null)
                {
                    string uploads = Path.Combine(hosting.WebRootPath, "uploads");
                    fileName = category.File.FileName;
                    //مسار الملف كاملا
                    string fullPath = Path.Combine(uploads, fileName);
                    // في ل fullPathحفظ الملف
                    string oldFileNAme = categoryRepository.Find(category.Id).ImageUrl;
                    string fullOldPath = Path.Combine(uploads, oldFileNAme);
                    if (fullPath != fullOldPath)
                    {
                        System.IO.File.Delete(fullOldPath);

                        category.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    }

                    category.ImageUrl = fileName;
                }

                categoryRepository.Update(id, category);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            var category = categoryRepository.Find(id);
            return View(category);
        }
        [HttpPost]
        public IActionResult Delete(int id, Category category)
        {
            try
            {
                categoryRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
}
}
