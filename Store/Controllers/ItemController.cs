﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;
using Store.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class ItemController : Controller
    {
        private readonly IElctronicStoreRepository<Item> itemRepository;
        private readonly IElctronicStoreRepository<Brand> brandRepository;
        private readonly IElctronicStoreRepository<Category> categoryRepository;
        private readonly IElctronicStoreRepository<BrandCategory> brandCategoryRepository;
        public ItemController(IElctronicStoreRepository<Item> itemRepository
            , IElctronicStoreRepository<Brand> brandRepository, 
            IElctronicStoreRepository<Category> categoryRepository,
             IElctronicStoreRepository<BrandCategory> brandCategoryRepository)
            
        {
            this.itemRepository = itemRepository;
            this.brandRepository = brandRepository;
            this.categoryRepository = categoryRepository;
            this.brandCategoryRepository = brandCategoryRepository;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var brands = itemRepository.List();
            return View(brands);
        }
        public IActionResult Details(int id)
        {
            var item = itemRepository.Find(id);

            return View(item);
        }

        public IActionResult Delete(int id)
        {
            var item = itemRepository.Find(id);
            return View(item);
        }
        [HttpPost]
        public IActionResult ConfirmDelete(int id)
        {
            try
            {
                itemRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Create()
        {
            var model = new ItemBrandCategoryViewModel
            {
                BrandCategorys = FillSelectList1(),
               Items= FillSelectList1().Select(ww=>new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
               {
                   Text=ww.Brand.Name,
                   Value=ww.Id.ToString(),
                   Group=new Microsoft.AspNetCore.Mvc.Rendering.SelectListGroup() 
                   {
                       Name=ww.Category.Name
                   }
               }).ToList(),
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(ItemBrandCategoryViewModel model)
        {
            
             var brandCategory = brandCategoryRepository.Find(model.BrandCategoryId);
            Item item = new Item()
            {
                Id = model.BrandCategoryId,
                BrandCategory=brandCategory,
              //  Brand = brand,
                //Category = category,
                Name=model.Name,
                Description=model.Description,
                Price=model.Price,
                Model=model.Model,
                Measure=model.Measure,Type=model.Type,
                RAM=model.RAM,VedioCard=model.VedioCard,
                Screen=model.Screen,CPU=model.CPU,SSD=model.SSD,HDD=model.HDD
            };
      
            itemRepository.Add(item);
            return RedirectToAction(nameof(Index));


        }
      
        List<BrandCategory> FillSelectList1()
        {
            var brandcategorys = brandCategoryRepository.List().ToList();
           // brandcategorys.Insert(0, new BrandCategory { Id = -1,  Name= "---Please select an brand---" });
            return brandcategorys;
        }
        List<Category> FillSelectList2()
        {
            var categorys = categoryRepository.List().ToList();
            categorys.Insert(0, new Category { Id = -1, Name = "---Please select an category---" });
            return categorys;
        }
       
    }
}
