﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class BillController : Controller
    {
        private readonly IElctronicStoreRepository<Bill> billRepository;

        public BillController(IElctronicStoreRepository<Bill> billRepository)
        {
            this.billRepository = billRepository;
        }
        public IActionResult Index()
        {
            var bills = billRepository.List();
            return View(bills);
        }
        public IActionResult Details(int id)
        {
            var bill = billRepository.Find(id);

            return View(bill);
        }
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public IActionResult Create(Bill bill)
        {
            if (ModelState.IsValid)
            {
                billRepository.Add(bill);
                return RedirectToAction(nameof(Index));
            }
            ModelState.AddModelError("", "You have to fill all required fields!");
            return View();

        }

        public IActionResult Edit(int id)
        {
            var bill = billRepository.Find(id);
            return View(bill);
        }
        [HttpPost]
        public IActionResult Edit(int id, Bill bill)
        {
            try
            {
               
                billRepository.Update(id, bill);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            var bill = billRepository.Find(id);
            return View(bill);
        }
        [HttpPost]
        public IActionResult Delete(int id, Bill bill)
        {
            try
            {
                billRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

    }
}
