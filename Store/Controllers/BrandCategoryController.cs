﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;
using Store.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class BrandCategoryController : Controller
    {
        private readonly IElctronicStoreRepository<BrandCategory> brandCategoryRepository;
        private readonly IElctronicStoreRepository<Brand> brandRepository;
        private readonly IElctronicStoreRepository<Category> categoryRepository;

        public BrandCategoryController(IElctronicStoreRepository<BrandCategory> brandCategoryRepository
            , IElctronicStoreRepository<Brand> brandRepository, IElctronicStoreRepository<Category> categoryRepository)
        {
            this.brandCategoryRepository = brandCategoryRepository;
            this.brandRepository = brandRepository;
            this.categoryRepository = categoryRepository;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var brandCategorys = brandCategoryRepository.List();
            return View(brandCategorys);
        }
        public IActionResult Details(int id)
        {
            var brandCategory = brandCategoryRepository.Find(id);

            return View(brandCategory);
        }
        public IActionResult Create()
        {
            var model = new BrandCategoryViewModel
            {
                Brands = FillSelectList1(),
               Categorys = FillSelectList2()
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(BrandCategoryViewModel model)
        {
            if (model.BrandId == -1)
            {
                ViewBag.Message = "Please select an brand from List!";
                var vmodel = new BrandCategoryViewModel
                {
                    Brands = FillSelectList1(),
                    Categorys=FillSelectList2()
                };
                return View(vmodel);
            }
            if (model.CategoryId == -1)
            {
                ViewBag.Message = "Please select an category from List!";
                var vmodel = new BrandCategoryViewModel
                {
                    Brands = FillSelectList1(),
                    Categorys = FillSelectList2()
                };
                return View(vmodel);
            }
            var brand = brandRepository.Find(model.BrandId);
            var category = categoryRepository.Find(model.CategoryId);
            BrandCategory brandCategory = new BrandCategory()
            {
                Id = model.BrandCategoryId,
                Brand=brand,
                Category=category
            };
                brandCategoryRepository.Add(brandCategory);
                return RedirectToAction(nameof(Index));
           

        }

        public IActionResult Edit(int id)
        {
            var brandCategory = brandCategoryRepository.Find(id);
            var brandId = brandCategory.Brand == null ? brandCategory.Brand.Id = 0 : brandCategory.Brand.Id;
            var categoryId = brandCategory.Category == null ? brandCategory.Category.Id = 0 : brandCategory.Category.Id;
            var viewModel = new BrandCategoryViewModel
            {
                BrandCategoryId=brandCategory.Id,
                BrandId=brandId,
                CategoryId=categoryId
                ,Brands= FillSelectList1(),
                Categorys= FillSelectList2()
            };
            return View(viewModel);
        }
        [HttpPost]
        public IActionResult Edit(BrandCategoryViewModel viewModel)
        {
            try
            {
                var brand = brandRepository.Find(viewModel.BrandId);
                var category = categoryRepository.Find(viewModel.CategoryId);
                BrandCategory brandCategory = new BrandCategory()
                {
                    //Id = viewModel.BrandCategoryId,
                    Brand = brand,
                    Category = category
                };
                brandCategoryRepository.Update(viewModel.BrandCategoryId, brandCategory);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            var brandCategory = brandCategoryRepository.Find(id);
            return View(brandCategory);
        }
        [HttpPost]
        public IActionResult ConfirmDelete(int id)
        {
            try
            {
                brandCategoryRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        List<Brand> FillSelectList1()
        {
            var brands = brandRepository.List().ToList();
            brands.Insert(0, new Brand { Id = -1,Name = "---Please select an brand---" });
            return brands;
        }
        List<Category> FillSelectList2()
        {
            var categorys = categoryRepository.List().ToList();
            categorys.Insert(0, new Category { Id = -1, Name = "---Please select an category---" });
            return categorys;
        }
     

    }
}
