﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;
using Store.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class BillItemController : Controller
    {
        private readonly IElctronicStoreRepository<BillItem> billItemRepository;
        private readonly IElctronicStoreRepository<Bill> billRepository;
        private readonly IElctronicStoreRepository<Item> itemRepository;

        public BillItemController(IElctronicStoreRepository<BillItem> billItemRepository,
            IElctronicStoreRepository<Bill> billRepository,
            IElctronicStoreRepository<Item> itemRepository)
        {
            this.billItemRepository = billItemRepository;
            this.billRepository = billRepository;
            this.itemRepository = itemRepository;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var billItems = billItemRepository.List();
            return View(billItems);
        }
        public IActionResult Details(int id)
        {
            var billItem = billItemRepository.Find(id);

            return View(billItem);
        }
        public IActionResult Create()
        {
            var model = new BillItemViewModel
            {
                Bills = FillSelectList1(),
                Items = FillSelectList2()
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(BillItemViewModel model)
        {
            if (model.BillId == -1)
            {
                ViewBag.Message = "Please select an brand from List!";
                var vmodel = new BillItemViewModel
                {
                    Bills = FillSelectList1(),
                    Items = FillSelectList2()
                };
                return View(vmodel);
            }
            if (model.ItemId == -1)
            {
                ViewBag.Message = "Please select an category from List!";
                var vmodel = new BillItemViewModel
                {
                    Bills = FillSelectList1(),
                    Items = FillSelectList2()
                };
                return View(vmodel);
            }
            var bill = billRepository.Find(model.BillId);
            var item = itemRepository.Find(model.ItemId);
            BillItem billitem = new BillItem()
            {
                Id = model.BillItemId,
                Bill = bill,
                Item = item
            };
            billItemRepository.Add(billitem);
            return RedirectToAction(nameof(Index));


        }
        public IActionResult Edit(int id)
        {
            var billItem =billItemRepository.Find(id);
            var billId = billItem.Bill == null ? billItem.Bill.Id = 0 : billItem.Bill.Id;
            var itemId = billItem.Item == null ? billItem.Item.Id = 0 : billItem.Item.Id;
            var viewModel = new BillItemViewModel
            {
                BillItemId = billItem.Id,
                BillId = billId,
                ItemId = itemId
                ,
                Bills = FillSelectList1(),
                Items = FillSelectList2()
            };
            return View(viewModel);
        }
        [HttpPost]
        public IActionResult Edit(BillItemViewModel viewModel)
        {
            try
            {
                var bill = billRepository.Find(viewModel.BillId);
                var item = itemRepository.Find(viewModel.ItemId);
                BillItem billItem = new BillItem()
                {
                    //Id = viewModel.BrandCategoryId,
                    Bill = bill,
                    Item = item
                };
               billItemRepository.Update(viewModel.BillItemId, billItem);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            var billItem =billItemRepository.Find(id);
            return View(billItem);
        }
        [HttpPost]
        public IActionResult ConfirmDelete(int id)
        {
            try
            {
                billItemRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        List<Bill> FillSelectList1()
        {
            var bills = billRepository.List().ToList();
            bills.Insert(0, new Bill { Id = -1, Date = "---Please select an brand---" });
            return bills;
        }
        List<Item> FillSelectList2()
        {
            var items = itemRepository.List().ToList();
            items.Insert(0, new Item { Id = -1, Name = "---Please select an category---" });
            return items;
        }

    }
}
