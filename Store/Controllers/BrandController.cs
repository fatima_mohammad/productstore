﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class BrandController : Controller
    {
        
        private readonly IElctronicStoreRepository<Brand> brandRepository;
        private readonly IHostingEnvironment hosting;

        public BrandController(IElctronicStoreRepository<Brand> brandRepository,
            IHostingEnvironment hosting)
        {
            this.brandRepository = brandRepository;
            this.hosting = hosting;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var brands = brandRepository.List();
            return View(brands);
        }
        public IActionResult Details(int id)
        {
            var brand = brandRepository.Find(id);

            return View(brand);
        }
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public IActionResult Create(Brand brand)
        {
           if(ModelState.IsValid)
            {
                string fileName=string.Empty;
               if(brand.File!=null)
                {
                    string uploads = Path.Combine(hosting.WebRootPath, "uploads");
                    fileName = brand.File.FileName;
                    //مسار الملف كاملا
                    string fullPath = Path.Combine(uploads, fileName);
                    // في ل fullPathحفظ الملف
                    brand.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    brand.ImageUrl = fileName;
                }
                brandRepository.Add(brand);
                return RedirectToAction(nameof(Index));
            }
            ModelState.AddModelError("", "You have to fill all required fields!");
            return View();
              
        }

        public IActionResult Edit(int id)
        {
            var brand= brandRepository.Find(id);
            return View(brand);
        }
        [HttpPost]
        public IActionResult Edit(int id, Brand brand)
        {
            try
            {
                string fileName = string.Empty;
                if (brand.File != null)
                {
                    string uploads = Path.Combine(hosting.WebRootPath, "uploads");
                    fileName = brand.File.FileName;
                    //مسار الملف كاملا
                    string fullPath = Path.Combine(uploads, fileName);
                    // في ل fullPathحفظ الملف
                    string oldFileNAme = brandRepository.Find(brand.Id).ImageUrl;
                    string fullOldPath = Path.Combine(uploads, oldFileNAme);
                    if(fullPath != fullOldPath)
                    {
                        System.IO.File.Delete(fullOldPath);

                        brand.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    }
                  
                    brand.ImageUrl = fileName;
                }

                brandRepository.Update(id, brand);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            var brand = brandRepository.Find(id);
            return View(brand);
        }
        [HttpPost]
        public IActionResult Delete(int id, Brand brand)
        {
            try
            {
                brandRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
