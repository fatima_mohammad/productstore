﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Repository;
using Store.ViewModels;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Store.Controllers
{
    public class ImageController : Controller
    {
        private readonly IElctronicStoreRepository<Image> imageRepository;
        private readonly IElctronicStoreRepository<Item> itemRepository;
        private readonly IHostingEnvironment hosting;

        public ImageController(IElctronicStoreRepository<Image> imageRepository,
            IElctronicStoreRepository<Item> itemRepository,
            IHostingEnvironment hosting)
        {
            this.imageRepository = imageRepository;
            this.itemRepository = itemRepository;
            this.hosting = hosting;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var images = imageRepository.List();
            return View(images);
        }
        public IActionResult Details(int id)
        {
            var image = imageRepository.Find(id);

            return View(image);
        }
        public IActionResult Create()
        {
            var model = new ImageItemViewModel
            {
               Items = FillSelectList1()
            
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(ImageItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.ItemId == -1)
                {
                    ViewBag.Message = "Please select an brand from List!";
                    var vmodel = new ImageItemViewModel
                    {
                        Items = FillSelectList1()
                    };
                    return View(vmodel);
                }

                string fileName = string.Empty;
                if (model.File != null)
                {
                    string uploads = Path.Combine(hosting.WebRootPath, "uploads");
                    fileName = model.File.FileName;
                    //مسار الملف كاملا
                    string fullPath = Path.Combine(uploads, fileName);
                    // في ل fullPathحفظ الملف
                    model.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    model.ImageUrl = fileName;
                }
                var item = itemRepository.Find(model.ItemId);
               
                Image image = new Image()
                {
                    Id = model.ImageId,
                   ImageUrl=model.ImageUrl,
                    Item = item
                };
                imageRepository.Add(image);
                return RedirectToAction(nameof(Index));
            }
            ModelState.AddModelError("", "You have to fill all required fields!");
            return View();

        }

        //public IActionResult Edit(int id)
        //{
        //    var image = imageRepository.Find(id);
        //    var itemId = image.Item == null ? image.Item.Id = 0 : image.Item.Id;
          
        //    var viewModel = new ImageItemViewModel
        //    {
        //        ImageId = image.Id,
        //        ItemId = itemId,
                
        //        Items = FillSelectList1()
              
        //    };
        //    return View(viewModel);
        //}
        //[HttpPost]
        //public IActionResult Edit(ImageItemViewModel viewModel)
        //{
        //    try
        //    {


        //        var item =itemRepository.Find(viewModel.ItemId);
             
        //        Image image = new Image()
        //        {
        //            //Id = viewModel.BrandCategoryId,
        //            Item = item,
        //            ImageUrl=viewModel.ImageUrl
                   
        //        };
        //        imageRepository.Update(viewModel.BrandCategoryId, brandCategory);
        //        return RedirectToAction(nameof(Index));


        //        string fileName = string.Empty;
        //        if (viewModel.File != null)
        //        {
        //            string uploads = Path.Combine(hosting.WebRootPath, "uploads");
        //            fileName = viewModel.File.FileName;
        //            //مسار الملف كاملا
        //            string fullPath = Path.Combine(uploads, fileName);
        //            // في ل fullPathحفظ الملف
        //            string oldFileNAme = itemRepository.Find(item.Id).ImageUrl;
        //            string fullOldPath = Path.Combine(uploads, oldFileNAme);
        //            if (fullPath != fullOldPath)
        //            {
        //                System.IO.File.Delete(fullOldPath);

        //                brand.File.CopyTo(new FileStream(fullPath, FileMode.Create));
        //            }

        //            brand.ImageUrl = fileName;
        //        }

        //        brandRepository.Update(id, brand);
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
        public IActionResult Delete(int id)
        {
            var image = imageRepository.Find(id);
            return View(image);
        }
        [HttpPost]
        public IActionResult Delete(int id, Brand brand)
        {
            try
            {
                imageRepository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        List<Item> FillSelectList1()
        {
            var items = itemRepository.List().ToList();
            items.Insert(0, new Item { Id = -1, Name = "---Please select an brand---" });
            return items;
        }
    }
  
}
