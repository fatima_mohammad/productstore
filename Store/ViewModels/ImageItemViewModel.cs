﻿using Microsoft.AspNetCore.Http;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.ViewModels
{
    public class ImageItemViewModel
    {
        public int ImageId { get; set; }
        public int ItemId { get; set; }
        public List<Item> Items { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile File { get; set; }
    }
}
