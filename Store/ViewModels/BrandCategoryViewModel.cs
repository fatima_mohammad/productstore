﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.ViewModels
{
    public class BrandCategoryViewModel
    {
        public int BrandCategoryId { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public List<Brand> Brands { get; set; }
        public List<Category> Categorys { get; set; }
    }
}
