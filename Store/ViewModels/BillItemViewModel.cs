﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.ViewModels
{
    public class BillItemViewModel
    {
        public int BillItemId { get; set; }
        public int ItemId { get; set; }
        public List<Item> Items { get; set; }
        public int BillId { get; set; }
        public List<Bill> Bills { get; set; }
        public int Quantity { get; set; }
    }
}
