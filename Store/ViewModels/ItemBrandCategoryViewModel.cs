﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.ViewModels
{
    public class ItemBrandCategoryViewModel
    {
        public int ItemId { get; set; }
        public int BrandCategoryId { get; set; }
        public List<BrandCategory> BrandCategorys { get; set; }
        public List<SelectListItem> Items { get; set; }


        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Model { get; set; }
        //w
        public string Measure { get; set; }
        //w
        public string Type { get; set; }
        //C
        public string RAM { get; set; }
        public string VedioCard { get; set; }
        public string Screen { get; set; }
        public string CPU { get; set; }
        public string SSD { get; set; }
        public string HDD { get; set; }
    }
}
