﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Store.Models;
using Store.Models.Repository;

namespace Store
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton<IElctronicStoreRepository<Brand>, BrandRepository>();
            services.AddSingleton<IElctronicStoreRepository<Category>, CategoryRepository>();
            services.AddSingleton<IElctronicStoreRepository<BrandCategory>, BrandCategoryRepository>();
            services.AddSingleton<IElctronicStoreRepository<Item>, ItemRepository>();
            services.AddSingleton<IElctronicStoreRepository<Image>, ImageRepository>();
            services.AddSingleton<IElctronicStoreRepository<Bill>, BillRepository>();
            services.AddSingleton<IElctronicStoreRepository<BillItem>, BillItemRepository>();
            services.AddDbContext<ItemStoreDBContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("SqlCon"));
            }
          );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc(route =>
            {
                route.MapRoute("default", "{controller=Brand}/{action=Index}/{id?}");
            });
            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});
            app.UseStaticFiles();
          //  app.UseMvcWithDefaultRoute();
        }
    }
}
