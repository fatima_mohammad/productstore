﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class BrandCategory
    {
        public int Id { get; set; }
        public Brand Brand { get; set; }
        public Category Category { get; set; }

    }
}
