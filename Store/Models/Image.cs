﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Image
    {
        public int Id { get; set; }
        public Item Item { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile File { get; set; }
    }
}
