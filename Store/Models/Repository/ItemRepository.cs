﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class ItemRepository : IElctronicStoreRepository<Item>
    {
        IList<Item>items;
        public ItemRepository()
        {
            items = new List<Item>()
            {
                new Item(){Id = 1, BrandCategory=new BrandCategory{Id=2},Name="k",
               Description="j",Price=8,Model="88", Measure="",Type="ch",RAM="n",VedioCard="",
                Screen="",CPU="",SSD="",HDD=""}


            };
        }
        public void Add(Item entity)
        {
            entity.Id = items.Max(b => b.Id) + 1;
            items.Add(entity);
        }

        public void Delete(int id)
        {
            var item = Find(id);
            items.Remove(item);
        }

        public Item Find(int id)
        {
            var item = items.SingleOrDefault(b => b.Id == id);
            return item;
        }

        public IList<Item> List()
        {
            return items;
        }

        //public List<Item> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Item newItem)
        {
            var item = Find(id);

            item.BrandCategory = newItem.BrandCategory;
            item.Name = newItem.Name;
            item.Description = newItem.Description;
            item.Price = newItem.Price;
            item.Model = newItem.Model;
            item.Measure = newItem.Measure;
            item.Type = newItem.Type;
            item.RAM = newItem.RAM;

            item.VedioCard = newItem.VedioCard;
            item.SSD = newItem.SSD;
            item.HDD = newItem.HDD;
            item.Screen = newItem.Screen;
            item.CPU = newItem.CPU;

        }
    }
}
