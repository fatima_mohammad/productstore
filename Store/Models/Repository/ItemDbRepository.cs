﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class ItemDbRepository : IElctronicStoreRepository<Item>
    {
        ItemStoreDBContext db;
        public ItemDbRepository(ItemStoreDBContext _db)
        {
            db = _db;
        }
        public void Add(Item entity)
        {
            entity.Id = db.Items.Max(b => b.Id) + 1;
            db.Items.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var item = Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
        }

        public Item Find(int id)
        {
            var item = db.Items.SingleOrDefault(b => b.Id == id);
            return item;
        }

        public IList<Item> List()
        {
            return db.Items.ToList();
        }

        //public List<Item> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Item newItem)
        {
            db.Update(newItem);
            db.SaveChanges();
        }
    }
}
