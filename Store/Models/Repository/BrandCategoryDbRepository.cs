﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BrandCategoryDbRepository : IElctronicStoreRepository<BrandCategory>
    {
        ItemStoreDBContext db;
        public BrandCategoryDbRepository(ItemStoreDBContext _db)
        {
            db = _db;  
        }
        public void Add(BrandCategory entity)
        {
            entity.Id = db.BrandCategorys.Max(b => b.Id) + 1;
            db.BrandCategorys.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var brandCategory = Find(id);
            db.BrandCategorys.Remove(brandCategory);
            db.SaveChanges();
        }

        public BrandCategory Find(int id)
        {
            var brandCategory = db.BrandCategorys.SingleOrDefault(b => b.Id == id);
            return brandCategory;
        }
        //public BrandCategory FindBC(int idb, int idc)
        //{
        //    var brandCategory = db.BrandCategorys.SingleOrDefault(b => b.Brand.Id == idb);
        //    return brandCategory;
        //}

        public IList<BrandCategory> List()
        {
            return db.BrandCategorys.ToList();
        }

        //public List<BrandCategory> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, BrandCategory newbrandCategorys)
        {
            db.Update(newbrandCategorys);
            db.SaveChanges();

        }
    }
}
