﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BillItemRepository : IElctronicStoreRepository<BillItem>
    {
        IList<BillItem> billItems;
        public BillItemRepository()
        {
            billItems = new List<BillItem>()
            {
                new BillItem
                {
                    Id=1 , Item=new Item{ Id=1} ,Bill=new Bill{ Id=1},Quantity=33

                }, new BillItem
                {
                  Id=2 ,  Item=new Item{ Id=1} ,Bill=new Bill{ Id=1},Quantity=33
                }
            };
        }
        public void Add(BillItem entity)
        {
            entity.Id = billItems.Max(b => b.Id) + 1;
            billItems.Add(entity);
        }

        public void Delete(int id)
        {
            var billItem = Find(id);
            billItems.Remove(billItem);
        }

        public BillItem Find(int id)
        {
            var billItem = billItems.SingleOrDefault(b => b.Id == id);
            return billItem;
        }

        public IList<BillItem> List()
        {
            return billItems;
        }

        //public List<BillItem> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, BillItem newBillitem)
        {
            var billItem = Find(id);

            billItem.Bill = newBillitem.Bill;
            billItem.Item = newBillitem.Item;
            billItem.Quantity = newBillitem.Quantity;
        }
    }
}
