﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class ImageRepository : IElctronicStoreRepository<Image>
    {
        IList<Image> images;
        public ImageRepository()
        {
            images = new List<Image>()
            {
                new Image{Id=1,Item=new Item{Id=1 } , ImageUrl="n.PNG"}
                

            };
        }
        public void Add(Image entity)
        {
            entity.Id = images.Max(b => b.Id) + 1;
            images.Add(entity);
        }

        public void Delete(int id)
        {
            var image = Find(id);
            images.Remove(image);
        }

        public Image Find(int id)
        {
            var image = images.SingleOrDefault(b => b.Id == id);
            return image;
        }

        public IList<Image> List()
        {
            return  images;
        }

        public void Update(int id, Image entity)
        {
            throw new NotImplementedException();
        }

        //public void Update(int id, Image entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public List<Image> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Update(int id, Image entity)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
