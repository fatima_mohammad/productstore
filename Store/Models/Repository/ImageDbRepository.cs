﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class ImageDbRepository : IElctronicStoreRepository<Image>
    {
        ItemStoreDBContext db;
        public ImageDbRepository(ItemStoreDBContext _db)
        {
            db = _db;
        }
        public void Add(Image entity)
        {
            entity.Id = db.Images.Max(b => b.Id) + 1;
            db.Images.Add(entity);
        }

        public void Delete(int id)
        {
            var image = Find(id);
            db.Images.Remove(image);
        }

        public Image Find(int id)
        {
            var image = db.Images.SingleOrDefault(b => b.Id == id);
            return image;
        }

        public IList<Image> List()
        {
            return db.Images.ToList();
        }

        public void Update(int id, Image entity)
        {
            throw new NotImplementedException();
        }

        //public void Update(int id, Image entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Update(int id, Image entity)
        //{
        //    throw new NotImplementedException();
        //}

        //public List<Image> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Update(int id, Image entity)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
