﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class CategoryDbRepository : IElctronicStoreRepository<Category>
    {
        ItemStoreDBContext db;

        public CategoryDbRepository(ItemStoreDBContext _db)
        {
            db = _db;
        }
        public void Add(Category entity)
        {
            entity.Id = db.Categorys.Max(b => b.Id) + 1;
            db.Categorys.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var brand = Find(id);
            db.Categorys.Remove(brand);
            db.SaveChanges();
        }

        public Category Find(int id)
        {
            var brand = db.Categorys.SingleOrDefault(b => b.Id == id);
            return brand;
        }

        public IList<Category> List()
        {
            return db.Categorys.ToList();
        }

        //public List<Category> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Category newCategory)
        {
            db.Update(newCategory);
            db.SaveChanges();
        }
    }
}
