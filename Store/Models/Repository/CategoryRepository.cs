﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class CategoryRepository : IElctronicStoreRepository<Category>
    {

        IList<Category> categorys;
        public CategoryRepository()
        {
            categorys = new List<Category>()
            {
                new Category
                {
                    Id=1 , Name="Dress" ,Description="Dress Beautiful"
                 ,Type="Wearing",ImageUrl="n.PNG"
                },  new Category
                {
                    Id=2 , Name="Laptop" ,Description="New Laptops"
                 ,Type="Electronic",ImageUrl="n.PNG"
                }
            };
        }
        public void Add(Category entity)
        {
            entity.Id = categorys.Max(b => b.Id) + 1;
            categorys.Add(entity);
        }

        public void Delete(int id)
        {
            var brand = Find(id);
            categorys.Remove(brand);
        }

        public Category Find(int id)
        {
            var brand = categorys.SingleOrDefault(b => b.Id == id);
            return brand;
        }

        public IList<Category> List()
        {
            return categorys;
        }

        //public List<Category> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Category newCategory)
        {
            var brand = Find(id);

            brand.Name = newCategory.Name;
            brand.Description = newCategory.Description;

            //  brand.ImageUrl = newBrand.ImageUrl;
            brand.Type = newCategory.Type;
            brand.ImageUrl = newCategory.ImageUrl;
        }
    }
}
