﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BrandRepository : IElctronicStoreRepository<Brand>
    {
        IList<Brand> brands;
        public BrandRepository()
        {
            brands = new List<Brand>()
            {
                new Brand
                {
                    Id=1 , Name="Microsoft" ,Description=" it's famous comapany"
                 ,Location="America",ImageUrl="n.PNG"
                }, new Brand
                {
                    Id=2 , Name="Google" ,Description=" it's famous comapany"
                 ,Location="America",ImageUrl="n.PNG"
                }
            };
        }
        public void Add(Brand entity)
        {
            entity.Id = brands.Max(b => b.Id) + 1;
            brands.Add(entity);
        }

        public void Delete(int id)
        {
            var brand = Find(id);
            brands.Remove(brand);
        }

        public Brand Find(int id)
        {
            var brand = brands.SingleOrDefault(b => b.Id == id);
            return brand;
        }

        public IList<Brand> List()
        {
            return brands;
        }

        //public List<Brand> Search(string term)
        //{
        //    //return brands.Where(b => b.Name.Contains(term)
        //    //   || b.Description.Contains(term)
        //    //   || b.Location.Contains(term)).ToList();

        //}

        public void Update(int id, Brand newBrand)
        {
            var brand = Find(id);

            brand.Name = newBrand.Name;
            brand.Description = newBrand.Description;
           
          //  brand.ImageUrl = newBrand.ImageUrl;
            brand.Location = newBrand.Location;
            brand.ImageUrl = newBrand.ImageUrl;
        }
    }
}
