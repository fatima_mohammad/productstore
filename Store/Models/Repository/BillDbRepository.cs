﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BillDbRepository : IElctronicStoreRepository<Bill>
    {
        ItemStoreDBContext db;
        public BillDbRepository(ItemStoreDBContext _db)
        {
            db = _db;   
        }
        public void Add(Bill entity)
        {
            entity.Id = db.Bills.Max(b => b.Id) + 1;
            db.Bills.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {

            var bill = Find(id);
            db.Bills.Remove(bill);
            db.SaveChanges();
        }

        public Bill Find(int id)
        {
            var bill = db.Bills.SingleOrDefault(b => b.Id == id);
            return bill;
        }

        public IList<Bill> List()
        {
            return db.Bills.ToList();
        }

        //public List<Bill> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Bill newBill)
        {
            db.Update(newBill);
            db.SaveChanges();
        }
    }
}
