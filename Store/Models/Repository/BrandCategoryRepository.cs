﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BrandCategoryRepository : IElctronicStoreRepository<BrandCategory>
    {
        IList<BrandCategory> brandCategorys;
        public BrandCategoryRepository()
        {
            brandCategorys = new List<BrandCategory>()
            {
                new BrandCategory(){Id = 1,Brand =new Brand{Id=2},Category=new Category{Id=2} }

                
            };
        }
        public void Add(BrandCategory entity)
        {
            entity.Id = brandCategorys.Max(b => b.Id) + 1;
            brandCategorys.Add(entity);
        }

        public void Delete(int id)
        {
            var brandCategory = Find(id);
            brandCategorys.Remove(brandCategory);
        }

        public BrandCategory Find(int id)
        {
            var brandCategory = brandCategorys.SingleOrDefault(b => b.Id == id);
            return brandCategory;
        }
        public BrandCategory FindBC(int idb,int idc)
        {
            var brandCategory = brandCategorys.SingleOrDefault(b => b.Brand.Id==idb);
            return brandCategory;
        }

        public IList<BrandCategory> List()
        {
            return brandCategorys;
        }

        //public List<BrandCategory> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, BrandCategory newbrandCategorys)
        {
            var brandCategory = Find(id);

            brandCategory.Brand = newbrandCategorys.Brand;
            brandCategory.Category = newbrandCategorys.Category;
           
          
        }
    }
}
