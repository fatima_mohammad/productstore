﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BillItemDbRepository : IElctronicStoreRepository<BillItem>
    {
        ItemStoreDBContext db;
        public BillItemDbRepository(ItemStoreDBContext _db)
        {
            db = _db;
        }
        public void Add(BillItem entity)
        {
            entity.Id = db.BillItems.Max(b => b.Id) + 1;
            db.BillItems.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var billItem = Find(id);
            db.BillItems.Remove(billItem);
            db.SaveChanges();
        }

        public BillItem Find(int id)
        {
            var billItem = db.BillItems.SingleOrDefault(b => b.Id == id);
            return billItem;
        }

        public IList<BillItem> List()
        {
            return db.BillItems.ToList();
        }

        //public List<BillItem> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, BillItem newBillitem)
        {
            db.Update(newBillitem);
            db.SaveChanges();

        }
    }
}
