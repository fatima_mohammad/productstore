﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BillRepository : IElctronicStoreRepository<Bill>
    {
        IList<Bill> bills;
        public BillRepository()
        {
            bills = new List<Bill>()
            {
                new Bill
                {
                    Id=1 , Date="2/1/2021" ,DeliverType="Flying"
                
                }, new Bill
                {
                  Id=2 , Date="Microsoft" ,DeliverType="Cars"
                }
            };
        }
        public void Add(Bill entity)
        {
            entity.Id = bills.Max(b => b.Id) + 1;
            bills.Add(entity);
        }

        public void Delete(int id)
        {

            var bill = Find(id);
            bills.Remove(bill);
        }

        public Bill Find(int id)
        {
            var bill = bills.SingleOrDefault(b => b.Id == id);
            return bill;
        }

        public IList<Bill> List()
        {
            return bills;
        }

        //public List<Bill> Search(string term)
        //{
        //    throw new NotImplementedException();
        //}

        public void Update(int id, Bill newBill)
        {
            var brand = Find(id);

            brand.Date = newBill.Date;
            brand.DeliverType = newBill.DeliverType;
        }
    }
}
