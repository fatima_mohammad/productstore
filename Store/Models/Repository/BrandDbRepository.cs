﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models.Repository
{
    public class BrandDbRepository: IElctronicStoreRepository<Brand>
    {
        ItemStoreDBContext db;
        public BrandDbRepository(ItemStoreDBContext _db)
        {
            db = _db;
        }
        public void Add(Brand entity)
        {
            entity.Id = db.Brands.Max(b => b.Id) + 1;
            db.Brands.Add(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var brand = Find(id);
            db.Brands.Remove(brand);
            db.SaveChanges();
        }

        public Brand Find(int id)
        {
            var brand = db.Brands.SingleOrDefault(b => b.Id == id);
            return brand;

        }

        public IList<Brand> List()
        {
            return db.Brands.ToList();
        }

        //public List<Brand> Search(string term)
        //{
        //    //return brands.Where(b => b.Name.Contains(term)
        //    //   || b.Description.Contains(term)
        //    //   || b.Location.Contains(term)).ToList();

        //}

        public void Update(int id, Brand newBrand)
        {
            db.Update(newBrand);
            db.SaveChanges();
        }
    }
}
