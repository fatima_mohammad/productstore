﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Brand
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        [MinLength(5)]
        public string Name { get; set; }
        [Required]
        [StringLength(120, MinimumLength = 5)]
        public string Description { get; set; }
       
        public string Location { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile File { get; set; }

    }
}
