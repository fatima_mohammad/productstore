﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class BillItem
    {
        public int Id { get; set; }
        public Item Item { get; set; }
        public Bill Bill { get; set; }
        public int Quantity { get; set; }
    }
}
