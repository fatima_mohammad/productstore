﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Bill
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string DeliverType { get; set; }
    }
}
